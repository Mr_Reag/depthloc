﻿namespace DepthCamForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.KinectCon = new System.Windows.Forms.Button();
            this.stopCapture = new System.Windows.Forms.Button();
            this.panAndZoomPictureBox1 = new Emgu.CV.UI.PanAndZoomPictureBox();
            this.renderBox = new Emgu.CV.UI.ImageBox();
            this.roiCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fromDistInput = new System.Windows.Forms.NumericUpDown();
            this.toDistInput = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.panAndZoomPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.renderBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromDistInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toDistInput)).BeginInit();
            this.SuspendLayout();
            // 
            // KinectCon
            // 
            this.KinectCon.Location = new System.Drawing.Point(13, 13);
            this.KinectCon.Name = "KinectCon";
            this.KinectCon.Size = new System.Drawing.Size(151, 87);
            this.KinectCon.TabIndex = 0;
            this.KinectCon.Text = "Connect to Kinect";
            this.KinectCon.UseVisualStyleBackColor = true;
            this.KinectCon.Click += new System.EventHandler(this.KinectCon_click);
            // 
            // stopCapture
            // 
            this.stopCapture.Location = new System.Drawing.Point(170, 13);
            this.stopCapture.Name = "stopCapture";
            this.stopCapture.Size = new System.Drawing.Size(172, 87);
            this.stopCapture.TabIndex = 3;
            this.stopCapture.Text = "Stop Capture";
            this.stopCapture.UseVisualStyleBackColor = true;
            this.stopCapture.Click += new System.EventHandler(this.StopCapture_click);
            // 
            // panAndZoomPictureBox1
            // 
            this.panAndZoomPictureBox1.Location = new System.Drawing.Point(830, 737);
            this.panAndZoomPictureBox1.Name = "panAndZoomPictureBox1";
            this.panAndZoomPictureBox1.Size = new System.Drawing.Size(8, 8);
            this.panAndZoomPictureBox1.TabIndex = 4;
            this.panAndZoomPictureBox1.TabStop = false;
            // 
            // renderBox
            // 
            this.renderBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.renderBox.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.RightClickMenu;
            this.renderBox.Location = new System.Drawing.Point(10, 100);
            this.renderBox.Name = "renderBox";
            this.renderBox.Size = new System.Drawing.Size(1600, 900);
            this.renderBox.TabIndex = 2;
            this.renderBox.TabStop = false;
            this.renderBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RenderBox_StartROICapture);
            this.renderBox.MouseLeave += new System.EventHandler(this.RenderBox_MouseLeave);
            this.renderBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RenderBox_StopROICapture);
            // 
            // roiCheckBox
            // 
            this.roiCheckBox.AutoSize = true;
            this.roiCheckBox.Checked = true;
            this.roiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.roiCheckBox.Location = new System.Drawing.Point(348, 13);
            this.roiCheckBox.Name = "roiCheckBox";
            this.roiCheckBox.Size = new System.Drawing.Size(295, 29);
            this.roiCheckBox.TabIndex = 5;
            this.roiCheckBox.Text = "Capture Region of Interest";
            this.roiCheckBox.UseVisualStyleBackColor = true;
            this.roiCheckBox.CheckedChanged += new System.EventHandler(this.ROICheckBox_Changed);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(675, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 80);
            this.label1.TabIndex = 8;
            this.label1.Text = "Capture Range,\r\nIn MM";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(866, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 75);
            this.label2.TabIndex = 9;
            this.label2.Text = "From\r\n\r\nTo";
            // 
            // fromDistInput
            // 
            this.fromDistInput.Location = new System.Drawing.Point(934, 13);
            this.fromDistInput.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.fromDistInput.Name = "fromDistInput";
            this.fromDistInput.Size = new System.Drawing.Size(120, 31);
            this.fromDistInput.TabIndex = 10;
            this.fromDistInput.ValueChanged += new System.EventHandler(this.DistInputBox_Changed);
            // 
            // toDistInput
            // 
            this.toDistInput.Location = new System.Drawing.Point(934, 57);
            this.toDistInput.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.toDistInput.Name = "toDistInput";
            this.toDistInput.Size = new System.Drawing.Size(120, 31);
            this.toDistInput.TabIndex = 11;
            this.toDistInput.Value = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.toDistInput.ValueChanged += new System.EventHandler(this.DistInputBox_Changed);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1678, 1134);
            this.Controls.Add(this.toDistInput);
            this.Controls.Add(this.fromDistInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.roiCheckBox);
            this.Controls.Add(this.renderBox);
            this.Controls.Add(this.panAndZoomPictureBox1);
            this.Controls.Add(this.stopCapture);
            this.Controls.Add(this.KinectCon);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.panAndZoomPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.renderBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromDistInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toDistInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button KinectCon;
        private System.Windows.Forms.Button stopCapture;
        private Emgu.CV.UI.PanAndZoomPictureBox panAndZoomPictureBox1;
        private Emgu.CV.UI.ImageBox renderBox;
        private System.Windows.Forms.CheckBox roiCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown fromDistInput;
        private System.Windows.Forms.NumericUpDown toDistInput;
    }
}

