﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Kinect;
using Emgu.CV;
using Emgu.CV.CvEnum;
using System.Runtime.InteropServices;
using Emgu.CV.UI;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DepthCamForm
{

    /// <inheritdoc />
    /// <summary>
    /// This class controls the depth camera form
    /// All code here operates under the assuption that this class is a singleton!
    /// </summary>
    public partial class Form1 : Form
    {
        //This is the delay for each draw command. We use it if we want a lower framerate
        //Used mainly for debugging
        private const int FrameSkipCount = 0;

        //Cutoff distance for sensor, in millimeters
        //private const int CutoffDist = 6000;

        //Here we define our sensors, sources, and readers
        private readonly KinectSensor sensor;
        private DepthFrameSource dSource;
        private DepthFrameReader dReader;

        //These are our data arrays. greyscaleData will be pinned to a specific chunk of memory
        private static ushort[] depthPoints;
        private static byte[] greyscaleData;

        //Width and height of the camera
        private static int height;
        private static int width;

        //What frame number we are
        private int frameCount = int.MaxValue;

        //Have we disposed the reader yet?
        private bool readerDisposed = false;

        //This will become the actual image we display
        private readonly Mat frame;

        private Point mousePoint;
        private bool captureROI;
        private static Rectangle regionOfInterest;

        /// <summary>
        /// Struct represents a depth range, with a simple mapping function included
        /// </summary>
        private struct FilterRange
        {
            public ushort from;
            public ushort to;

            /// <summary>
            /// Maps the input depth short to a color if its out of bounds. Otherwise, returns 1
            /// </summary>
            /// <param name="d">The ushort representing the distance in MM</param>
            /// <returns>0 or 255 if d is out of range, 1 otherwise</returns>
            public byte MapRange(ushort d)
            {
                byte r = 255; //default => too close -> map to white
                if (d > to | d == 0) r = 0; // if we're too far -> map to black
                else if (d > from) r = 1;// otherwise, we're good -> map to accept
                return r;
            }
        }

        private static FilterRange filterRange;


        public Form1()
        {
            InitializeComponent();

            //Setup sensor
            sensor = KinectSensor.GetDefault();
            height = sensor.DepthFrameSource.FrameDescription.Height;
            width = sensor.DepthFrameSource.FrameDescription.Width;

            //Create an array for the depth point cloud
            //Each point is the distance of that pixel in millimeters. 
            depthPoints = new ushort[height * width];

            //Create shared memory access for image and greyscale
            //This way, if we edit the value of greyscaleData, it will automatically be passed to frame!
            greyscaleData = new byte[height * width];
            var greyHandle = GCHandle.Alloc(greyscaleData, GCHandleType.Pinned);

            //Create a new material with our pinned data
            frame = new Mat(height, width, DepthType.Cv8U, 1, greyHandle.AddrOfPinnedObject(), 0);
            renderBox.Image = frame;
            renderBox.Width = width;
            renderBox.Height = height;

            //Disable zoom and mouse features. We will use the mouse to select the ROI later
            renderBox.FunctionalMode = ImageBox.FunctionalModeOption.RightClickMenu;

            //Set up our initial filter range and region of interest (Default values and entire screen)
            filterRange.from = Convert.ToUInt16(fromDistInput.Value);
            filterRange.to = Convert.ToUInt16(toDistInput.Value);
            regionOfInterest = new Rectangle(0, 0, width, height);
        }

        //Called when the connect button is called
        private void KinectCon_click(object sender, EventArgs e)
        {
            //Open the sensor and get all the sources and readers
            if (sensor.IsOpen) return;
            sensor.Open();

            dSource = sensor.DepthFrameSource;
            dReader = dSource.OpenReader();

            //Assign the FrameArrived event. This way, when the camera updates, we will draw to the screen
            dReader.FrameArrived += DSource_FrameArrived;
            readerDisposed = false;
        }

        //This function is called every frame of the kinect camera
        private void DSource_FrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            UpdateBitmap();
        }

        //Takes a distance and a pixel location, and updates the frame to the mapped value
        //Note: We have a rounding error of about 1/scale. By default, that is 1/.0425 or 23.5 millimeters
        /*private static void MapDepthToGrey(ushort dPoint, int pixelId)
        {
            byte c = 255;
            if (dPoint < CutoffDist)
            {
                const double scale = 255.0 / CutoffDist; //White (255) divided by cutoff. .0425 by default
                c = Convert.ToByte((dPoint * scale));
                //c = Convert.ToByte(c * scale);
            }
            greyscaleData[pixelId] = c;
        }*/


        private static void FilterAndMapDepthToGrey(ushort dPoint, int pixelId)
        {
            byte c = filterRange.MapRange(dPoint); //map the byte to see if its in our filter
            //If we map to 1, we're in the filter!

            //if we're in the filter
            if (c == 1)
            {
                //And if we're in the ROI
                int x = pixelId % width;
                int y = pixelId / width;
                var p = new Point(x, y);

                //scale and map it!
                if (regionOfInterest.Contains(p))
                {
                    double scale = 255.0 / (filterRange.to - filterRange.from);
                    c = Convert.ToByte(255 - (dPoint - filterRange.from) * scale);

                    /*if (c == 1)
                    {
                        Console.Out.WriteLine("Scale: " + scale + ", dPoint: " + dPoint + ", C:" + c);
                    }*/ //debug
                }
            }

            //And update the filter textures color
            greyscaleData[pixelId] = c;
        }


        //Aquire this lock every time you need to update the frame!
        private readonly object frameLock = new object();


        //Updates the bitmap we use to render the depth camera
        private void UpdateBitmap()
        {
            
            //Ensure we only have one process updating the bitmap at a time
            if (!Monitor.TryEnter(frameLock)) return;      
            try
            {               
                //Our frameskip
                if (frameCount >= FrameSkipCount)
                {
                    frameCount = 0;
                }
                else
                {
                    frameCount++;
                    return;
                }
                Stopwatch watch = new Stopwatch();
                watch.Start();
                if (!sensor.IsOpen) return;

                //Get the latest frame and copy it into our array
                var dFrame = dReader.AcquireLatestFrame();
                if (dFrame == null) return;
                dFrame.CopyFrameDataToArray(depthPoints);

                //Map our depth data to the frame

                //This block is less efficient than the one below. Re-enable if parallelism issues
                /*for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        int depthId = i * width + j;
                        FilterAndMapDepthToGrey(depthPoints[depthId], depthId);
                    }
                }*/

                //Use parallelism to increase efficiency for lower end devices! Tested as being more efficient than the flat loop
                Parallel.For(0,depthPoints.Length, index =>
                {
                    FilterAndMapDepthToGrey(depthPoints[index],index);
                });

                //Finally, update the render image and dispose of the current depth frame
                renderBox.Image = frame;
                dFrame.Dispose();
                watch.Stop();
                Console.Out.WriteLine("Ticks: " + watch.ElapsedTicks);
            }
            finally
            {
                //Release the lock
                Monitor.Exit(frameLock);

                
            }
        }



        //Called when Stop Capture is clicked
        private void StopCapture_click(object sender, EventArgs e)
        {
            try
            {
                Monitor.Enter(frameLock);
                if (readerDisposed) return;
                dReader.FrameArrived -= DSource_FrameArrived;
                dReader.Dispose();
                sensor.Close();
                readerDisposed = true;

                //Then blank the frame
                BlankFrame();
            }
            catch (Exception err)
            {
                Console.Error.WriteLine(err.ToString());
            }
            finally
            {
                //release the lock on the frame
                Monitor.Exit(frameLock);
            }
        }

        //fill the grayscaleData array with black
        private void BlankFrame()
        {
            try
            {
                const byte v = 0;
                for (int i = 0; i < greyscaleData.Length; i++)
                {
                    greyscaleData[i] = v;
                }
            }
            finally
            {
                renderBox.Image = frame;
            }
        }

        //We use this to tell if we need to start capturing for the ROI
        private void ROICheckBox_Changed(object sender, EventArgs e)
        {
            var state = roiCheckBox.CheckState;
            if (state != CheckState.Checked)
            {
                SetROICaptureAsInvalid();
            }
        }

        private void RenderBox_StartROICapture(object sender, MouseEventArgs e)
        {
            var state = roiCheckBox.CheckState;
            if (captureROI || state != CheckState.Checked) return;

            captureROI = true;


            mousePoint = e.Location;
            if (mousePoint.X > width) mousePoint.X = width;
            if (mousePoint.X < 0) mousePoint.X = 0;
            if (mousePoint.Y > height) mousePoint.Y = height;
            if (mousePoint.Y < 0) mousePoint.Y = 0;
        }

        //Once we're done capturing, calculate the area we want to pay attention too
        private void RenderBox_StopROICapture(object sender, MouseEventArgs e)
        {
            if (!captureROI) return;

            var other = e.Location;
            if (other.X > width) other.X = width;
            if (other.X < 0) other.X = 0;
            if (other.Y > height) other.Y = height;
            if (other.Y < 0) other.Y = 0;

            int xLen = Math.Abs(mousePoint.X - other.X);
            int yLen = Math.Abs(mousePoint.Y - other.Y);

            if (xLen == 0 || yLen == 0) return;

            bool leftSide = mousePoint.X < other.X;
            bool topSide = mousePoint.Y < other.Y;
            Point topLeftPoint;

            //TopLeft or BottomRight box built of of init mouse pos
            if (leftSide == topSide) topLeftPoint = (leftSide) ? mousePoint : other;
            else topLeftPoint = (leftSide) ? new Point(mousePoint.X, other.Y) : new Point(other.X, mousePoint.Y);

            //Create our ROI
            regionOfInterest = new Rectangle(topLeftPoint, new Size(xLen, yLen));

            captureROI = false;
        }

        private void RenderBox_MouseLeave(object sender, EventArgs e)
        {
            SetROICaptureAsInvalid();
        }


        private void SetROICaptureAsInvalid()
        {
            captureROI = false;
            mousePoint = new Point(-1, -1); //Invalid point
        }

        private void DistInputBox_Changed(object sender, EventArgs e)
        {
            ushort from = Convert.ToUInt16(fromDistInput.Value);
            ushort to = Convert.ToUInt16(toDistInput.Value);

            if (from >= to)
            {
                filterRange.from = 0;
                filterRange.to = ushort.MaxValue;
            }
            else
            {
                filterRange.from = from;
                filterRange.to = to;
            }
        }
    }
}
